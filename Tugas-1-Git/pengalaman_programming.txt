Study Independent Alterra Academy February 2022 – Present
(Vue JS Front-End Engineer)
 Understanding Front-End, Javascript, Vue.JS Fundamental
 Connect API to Vue.JS
 Make Mini Project with Vue.JS (Todo List)
 Vue CLI and Vue With Nuxt

Information System Project Management Class August – December 2021 (Project Manager)
 Make a project plan, starting from determining scope, timeline, resource, to the main goals to be achieved
 Allocating task Units to the team with Microsoft Project
 Forming effective team communication
 Monitoring the progress of the project running according to the specified time using git

Sekarang kuliah di Universitas Negeri Surabaya Jurusan Sistem Informasi Semester 6
